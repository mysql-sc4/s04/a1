-- Activity


-- Find all artists that has letter d in its name.
SELECT * FROM artists WHERE artists.name REGEXP '[d]';

-- Find All songs that has a length of less than 230.

SELECT * FROM songs WHERE length < 230;

-- Join the 'albums' and 'songs' tables. (Only Show the album name, song name, and song length)

SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name)

SELECT * FROM albums 
	RIGHT JOIN artists ON albums.artist_id = artists.id 
	WHERE albums.album_title REGEXP '[a]';


-- Sort the albums in Z-A order. (Show only the first 4 records)

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the 'albums' and 'songs' tables (Sort albums from Z-A and sort songs from A-Z).

SELECT * FROM songs
	JOIN albums ON songs.album_id = albums.id
	ORDER BY song_name ASC, album_title DESC;
